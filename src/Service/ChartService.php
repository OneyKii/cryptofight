<?php

namespace App\Service;

use DateTime;
use Symfony\UX\Chartjs\Model\Chart;
use Codenixsv\CoinGeckoApi\CoinGeckoClient;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;

class ChartService 
{

    private $chartBuild;
    private $coinGeckoClient;
    private $crypto  = array('bitcoin', 'ethereum', 'monero', 'solana');
    private $img = array('btc.webp', 'eth.webp', 'xmr.webp', 'sol.webp');

    public function __construct(ChartBuilderInterface $chartBuilder)
    {
        $this->chartBuild = $chartBuilder;
        $this->coinGeckoClient = new CoinGeckoClient();
    }

    public function displayChart()  
    {
        // init DateTime & CoinGeckoClient
        $date = new \DateTime();

        $day = '30'; //array('15', '30', '60', '90');
        $startTimeStamp = new \DateTime('now - ' . $day . ' day');

        $crypto = $this->crypto; // tableau avec array, peut faire un tableau vide également "array()"
        $chartPlayer = []; // tableau avec accolades

        // for ($j = 0; $j < count($crypto); $j++) {
        for ($j = 0; $j < 1; $j++) {
            
            // $result = $this->coinGeckoClient->coins()->getMarketChartRange($crypto[$j], 'usd', $startTimeStamp->getTimestamp(), $date->getTimestamp())['prices'];
            $result = $this->coinGeckoClient->coins()->getMarketChartRange(
                'ethereum', // nom de la crypto
                'usd', // devise de la crypto
                $startTimeStamp->getTimestamp(), // timestamp actuel - 30 jours
                $date->getTimestamp() // timestamp actuel
                )['prices']; 

                
            
            $labelInArray = [];
            $dataInArray = [];


            for ($i = 0; $i < count($result); $i++) {
                if ($i % 24 === 0) {
                    // array push & [] après $dataInArray sont 2 techniques pour remplir le tableau
                    array_push($labelInArray, $result[$i][0]);
                    $dataInArray[] = $result[$i][1];
                }
            }
            
            $chartPlayer[$j] = $this->chartBuild->createChart(Chart::TYPE_LINE);
            $chartPlayer[$j]->setData([
                'labels' => $labelInArray,
                'datasets' => [
                    [
                        //'label' => $crypto[$j],
                        'label' => 'ethereum',
                        'backgroundColor' => 'rgb(254, 231, 21)',
                        'borderColor' => 'rgb(254, 231, 21)',
                        'data' => $dataInArray,
                        'tension' => 0.4,
                    ],
                ],
            ]);
            
            $chartPlayer[$j]->setOptions([
                'scales' => [
                    'y' => [
                        'beginAtZero' => true,
                    ],
                ],
            ]);
        }

        return [
            'chartPlayer' => $chartPlayer,
            'cryptos' => $crypto,
        ];
    }



    public function displayChartTwo()  
    {
        // init DateTime & CoinGeckoClient
        $date = new \DateTime();

        $day = '30'; //array('15', '30', '60', '90');
        $startTimeStamp = new \DateTime('now - ' . $day . ' day');

        $crypto = $this->crypto; // tableau avec array, peut faire un tableau vide également "array()"
        $chartPlayerTwo = []; // tableau avec accolades

        // for ($j = 0; $j < count($crypto); $j++) {
        for ($j = 0; $j < 1; $j++) {
            
            // $result = $this->coinGeckoClient->coins()->getMarketChartRange($crypto[$j], 'usd', $startTimeStamp->getTimestamp(), $date->getTimestamp())['prices'];
            $resultTwo = $this->coinGeckoClient->coins()->getMarketChartRange('solana', 'usd', $startTimeStamp->getTimestamp(), $date->getTimestamp())['prices'];
            
            $labelInArray = [];
            $dataInArray = [];


            // for ($i = 0; $i < count($resultTwo); $i++) {
            for ($i = 0; $i < 29; $i++) {
                if ($i % 1 === 0) {
                    // array push & [] après $dataInArray sont 2 techniques pour remplir le tableau
                    $labelInArray[] = $resultTwo[$i][0];
                    $dataInArray[] = $resultTwo[$i][1];
                    // dd($labelInArray, $dataInArray);
                }
            }
            
            $chartPlayerTwo[$j] = $this->chartBuild->createChart(Chart::TYPE_LINE);
            $chartPlayerTwo[$j]->setData([
                //'labels' => $labelInArray,
                'labels' => $labelInArray,
                'datasets' => [
                    [
                        //'label' => $crypto[$j],
                        'label' => 'solana',
                        'backgroundColor' => 'rgb(254, 231, 21)',
                        'borderColor' => 'rgb(254, 231, 21)',
                        'data' => $dataInArray,
                        'tension' => 0.4,
                    ],
                ],
            ]);
            
            $chartPlayerTwo[$j]->setOptions([
                'scales' => [
                    'y' => [
                        'beginAtZero' => true,
                    ],
                ],
            ]);
        }

        return [
            'chartPlayerTwo' => $chartPlayerTwo,
            'cryptos' => $crypto,
        ];
    }





    function displayCrypto() 
    {
        $crypto = $this->crypto;
        $img = $this->img;
        $arrayCryptos = array();

        for ($i = 0; $i < count($crypto); $i++) {
            $arrayRst = $this->coinGeckoClient->simple()->getPrice($crypto[$i], 'usd');
            $valuesUSD = $arrayRst[$crypto[$i]]["usd"];
            $newCrypto = array(
                'label' => $crypto[$i],
                'valuesUSD' => $valuesUSD,
                'img' => $img[$i]
            );
            array_push($arrayCryptos, $newCrypto);
        }

        return [
            'arrayCryptos' => $arrayCryptos,
            'newCrypto' => $newCrypto,
        ]; 
    }
    // TODO : transformer les for en forEach, 
}