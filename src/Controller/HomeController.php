<?php

namespace App\Controller;


use App\Entity\User;
use App\Service\ChartService;
use Symfony\UX\Chartjs\Model\Chart;
use Codenixsv\CoinGeckoApi\CoinGeckoClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(ChartService $chartService): Response
    {
        $chart = $chartService->displayChart();

        $chartTwo = $chartService->displayChartTwo();
        
        $user = new User();
        $name = $user->getUsername();

        $list = $chartService->displayCrypto();

        return $this->render('home/index.html.twig', [
            'chartPlayer' => $chart["chartPlayer"],
            'chartPlayerTwo' => $chartTwo["chartPlayerTwo"],
            'cryptos' => $chart["cryptos"],
            'arrayCryptos' => $list["arrayCryptos"],
            'name' => $name,
        ]);
    }
}
