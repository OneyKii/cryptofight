<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MetadamasController extends AbstractController
{
    #[Route('/metadamas', name: 'app_metadamas')]
    public function index(): Response
    {
        $metadataNftCollection = json_decode(file_get_contents('js/json/_metadata.json'), true);

        return $this->render('metadamas/index.html.twig', [
            'metadataNftCollection' => $metadataNftCollection,
        ]);
    }
}
