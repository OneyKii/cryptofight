const navBtn = document.querySelector(".nav-btn");
const closeBtn = document.querySelector(".close-btn");
const nav = document.querySelector(".navigation");
const header = document.querySelector(".header");

// open navigation when menu button is clicked
  navBtn.addEventListener("click", (e) => {
    e.stopPropagation();
    nav.classList.add("navigation-active");
  
    // close navigation when clicked off
    document.addEventListener("click", (e) => {
      if (e.target != nav) {
        nav.classList.remove("navigation-active");
      }
    });
  });
  
  // close navigation when x is clicked
  closeBtn.addEventListener("click", () => {
    nav.classList.remove("navigation-active");
  });

